from django.contrib import admin
from .models import Article, Ad, Currency

# Register your models here.


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'publish', 'status')
    list_filter = ('publish', 'status')
    search_fields = ['title', 'slug', 'publish', 'status']
    prepopulated_fields = {('slug'): ("title",)}
    ordering = ["status", "publish"]


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle', 'thumbnail', 'startTime', 'endTime')
    list_filter = ('startTime', 'endTime')
    search_fields = ['title', 'endTime']
    ordering = ["startTime"]


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('currency', 'price')
    list_filter = ('currency', 'price')
    search_fields = ['currency']
    ordering = ["currency"]


# mod = (Article, Ad, Currency)
# for m in mod:
#     admin.site.register(Article, Ad, Currency)


# admin.site.register(Article, Ad, Currency(((())))
