from django.shortcuts import render
from .models import Article, Ad, Currency

# Create your views here.


def home(request):
    context = {
        "articles": Article.objects.filter(status="p").order_by("publish")[:3],
        "ads": Ad.objects.all(),
        # faghat alhari namayesh dade mishe
        "currences": Currency.objects.all()
    }
    return render(request, "blog/home.html", context)


def detial(request, slug):
    context = {
        "article": Article.objects.get(slug=slug)
    }
    return render(request, "blog/single.html", context)
