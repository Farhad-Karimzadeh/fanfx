from django.db import models
from django.utils import timezone

# Create your models here.


class Article(models.Model):
    STATUS_CHOICES = (
        ("p", "published"),
        ("d", "draft")
    )
    title = models.CharField(max_length=50, verbose_name="عنوان")
    slug = models.SlugField(max_length=50, unique=True, verbose_name="لینک")
    description = models.TextField(verbose_name="مقاله")
    thumbnail = models.ImageField(upload_to="images", verbose_name="عکس")
    publish = models.DateTimeField(
        default=timezone.now, verbose_name="تاریخ شروع")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="تاریخ تولید")
    update = models.DateTimeField(auto_now=True, verbose_name="تاریخ ویرایش")
    status = models.CharField(
        max_length=1, choices=STATUS_CHOICES, verbose_name="وضعیت")

    def __str__(self):
        return self.title


class Ad(models.Model):
    title = models.CharField(max_length=50, verbose_name="عنوان")
    subtitle = models.CharField(max_length=100, verbose_name="توضیح کوتاه")
    thumbnail = models.ImageField(upload_to="adImg", verbose_name="بنر")
    startTime = models.DateTimeField(
        auto_now_add=True, verbose_name="تاریخ شروع تبلیغ")
    endTime = models.DateField(verbose_name="تاریخ پایان تبلیغ")

    def __str__(self):
        return self.title


class Currency(models.Model):
    currency = models.CharField(max_length=50, verbose_name="ارز")
    price = models.CharField(max_length=50, verbose_name="قیمت")

    def __str__(self):
        return self.currency
