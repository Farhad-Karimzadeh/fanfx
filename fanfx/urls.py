from django.contrib import admin
from django.urls import path
from article.views import home, detial
from django.conf import settings
from django.conf.urls.static import static

app_name = "fanfx"
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name="home"),
    path('article/<slug:slug>', detial, name="detail"),
]
urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)